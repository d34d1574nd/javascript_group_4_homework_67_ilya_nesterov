import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Password.css';

class Password extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-descr">
                    <p className={this.props.access}>{this.props.star}</p>
                    <span>{this.props.text}</span>
                    <div className='button'>
                        <button onClick={() => this.props.inputNumber(1)}>1</button>
                        <button onClick={() => this.props.inputNumber(2)}>2</button>
                        <button onClick={() => this.props.inputNumber(3)}>3</button>
                        <button onClick={() => this.props.inputNumber(4)}>4</button>
                        <button onClick={() => this.props.inputNumber(5)}>5</button>
                        <button onClick={() => this.props.inputNumber(6)}>6</button>
                        <button onClick={() => this.props.inputNumber(7)}>7</button>
                        <button onClick={() => this.props.inputNumber(8)}>8</button>
                        <button onClick={() => this.props.inputNumber(9)}>9</button>
                        <button onClick={this.props.delete}>&lt;</button>
                        <button onClick={() => this.props.inputNumber(0)}>0</button>
                        <button onClick={this.props.inCorrect}>E</button>
                    </div>
                </div>
            </div>
        );
    }
};

const mapStateToProps = state => {
    return {
        star: state.star,
        access: state.access,
        text: state.text
    };
};

const mapDispatchToProps = dispatch => {
    return {
        inputNumber: number => dispatch({type: 'INPUT_NUMBER', number}),
        delete: () => dispatch({type: 'DELETE_NUMBER'}),
        inCorrect: () => dispatch({type: 'CORRECT'}),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);