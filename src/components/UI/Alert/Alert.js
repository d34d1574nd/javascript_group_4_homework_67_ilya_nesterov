import React, {Fragment} from 'react';
import './Alert.css'

const Alert = props => {
  return (
    <Fragment>
      <div
        className={props.type}
        typeof={props.type}
        onClick={props.close}
        style={{
          transform: props.show ? 'translateY(0)': 'translateY(-100vh)',
          opacity: props.show ? '1': '0'
        }}
      >
        <p className='someText'>{props.children}</p>
        <button className={props.class} onClick={props.button}>x</button>
      </div>
    </Fragment>
  )
};

export default Alert;