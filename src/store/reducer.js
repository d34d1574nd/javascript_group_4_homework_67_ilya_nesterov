const initialState = {
    password: '',
    star: '',
    currentPassword: 1791,
    access: 'notPassword',
    text: ''
};

const reducer = (state = initialState, action) => {
    if (action.type === 'DELETE_NUMBER') {
        const newPassword = state.password.substr(0, state.password.length - 1);
        const newStar = state.star.substr(0, state.star.length - 1);
        return {
            ...state,
            password: state.password = newPassword,
            star: state.star = newStar,
            access: state.access = 'notPassword',
            text: state.text = '',
        };
    }
    if (action.type === "CORRECT") {
        if (+state.password === state.currentPassword) {
            return {
                ...state,
                access: state.access = 'access',
                text: state.text = 'Access Granted'
            }
        } else {
            return {
                ...state,
                access: state.access = 'notAccess',
                text: state.text = 'Not authorized'
            }
        }
    }
    if (state.star.length === 4) {
        return {
            ...state,
        }
    }
    if (action.type === 'INPUT_NUMBER') {
        return {
            ...state,
            password: state.password + action.number,
            star: state.star + '*'
        };
    }
    return state;
};

export default reducer;